(function () {

    var inIframe = function () {
        try {
            return (window.self !== window.top) ? 1 : 0;
        } catch (e) {
            return 1;
        }
    }
            
    var getPageUrl = function () {
        var referrer = window.location.toString();
        if (inIframe()) {
            console.log('in ifr');
            try {
                referrer = window.top.location.toString();
            } catch (e) {
                console.log('caught err');
                console.log(e);
                referrer = '';
            }
        }
        return referrer;
    }
    
    // Inside an <iframe>, the Document.referrer will initially be set to the same value as the href of the parent window's Window.location.
    var getPageReferrer = function () {
        var pageRef = document.referrer;
        if (pageRef) {
            return pageRef;
        }
        return '';
    }
    
    var getReferrer = function () {
        var referrer = document.referrer;
        if (inIframe()) {
            console.log('in ifr');
            console.log(window.location.ancestorOrigins);
            try {
                referrer = window.top.document.referrer;
            }
            catch (e) {
                console.log('there was err');
                console.log(e);
                referrer = '';
            }
        }
        return referrer;
    };
        
    var parentPageUrl = function() {
        return (window.location != window.parent.location)
            ? document.referrer
            : document.location.href;
    }
    
    var run = function () {
       console.log('running');
       
       console.log('ref: ' + getPageUrl());
       console.log('page ref: ' + getPageReferrer());
       console.log('parent page: ' + parentPageUrl());
       console.log('old ref retr: ' + getReferrer());
    }

    var init = function () {
        if (document.readyState === 'complete' || document.readyState === 'interactive') {
            run();
            return;
        }
        setTimeout(init, 100);
    }

    init();
})();